<?php

namespace Database\Factories;

use App\Contracts\StoreContract;
use App\Models\Store;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class StoreFactory extends Factory
{

    protected $model = Store::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
            StoreContract::DESCRIPTION => $this->faker->text,
            StoreContract::NAME => $this->faker->name,
            StoreContract::USER_ID => User::inRandomOrder()->first()->id,
            StoreContract::PHOTO => $this->faker->imageUrl
        ];
    }
}
