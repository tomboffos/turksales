<?php


namespace App\Repositories;


use App\Http\Requests\Auth\RegisterRequest;
use App\Models\User;
use App\Repositories\Interfaces\UserRepositoryInterface;
use App\Services\DeviceService;

class UserRepository implements UserRepositoryInterface
{
    protected $deviceService;

    public function __construct()
    {
        $this->deviceService = new DeviceService();
    }

    public function create(RegisterRequest $request)
    {
        $user = User::create(array_merge($request->validated(),[ 'role_id' => 1]));

        $this->deviceService->registerDevice($request['device_token'], $user);

        return $user;
    }
}
