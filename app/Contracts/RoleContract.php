<?php


namespace App\Contracts;


interface RoleContract
{
    const NAME = 'name';

    const FILLABLE = [
        self::NAME
    ];

    const ADMIN = 'admin';
    const CUSTOMER = 'customer';
    const SELLER = 'seller';

    const TYPES = [
        [
            'id' => 1,
            self::NAME => self::ADMIN
        ],
        [
            'id' => 2,
            self::NAME => self::CUSTOMER
        ],
        [
            'id' => 3,
            self::NAME => self::SELLER
        ],
    ];
}
