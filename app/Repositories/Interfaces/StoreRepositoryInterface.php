<?php


namespace App\Repositories\Interfaces;


use App\Http\Requests\Store\StoreRequest;
use App\Models\Store;

interface StoreRepositoryInterface
{
    public function create(StoreRequest $request);

    public function update(StoreRequest $request, Store $store);

    public function delete(Store $store);
}
