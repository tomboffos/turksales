<?php

use App\Contracts\ProductContract;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->foreignId(ProductContract::STORE_ID)->constrained();
            $table->foreignId(ProductContract::CATEGORY_ID)->constrained();
            $table->string(ProductContract::NAME);
            $table->text(ProductContract::DESCRIPTION)->nullable();
            $table->text(ProductContract::IMAGES);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
