<?php

namespace App\Http\Controllers\Api\Store;

use App\Contracts\StoreContract;
use App\Http\Controllers\Controller;
use App\Http\Requests\Store\StoreRequest;
use App\Http\Resources\Api\Store\StoreResource;
use App\Models\Store;
use App\Repositories\StoreRepository;
use Illuminate\Http\Request;

class StoreController extends Controller
{
    protected $storeRepository;

    public function __construct()
    {
        $this->storeRepository = new StoreRepository();
    }

    public function showOwnStore(Request $request)
    {
        return new StoreResource($request->user()->store);
    }

    public function index(Request $request)
    {
        return StoreResource::collection(Store::where(function ($query) use ($request){
            if ($request->has('search'))
                $query->where(StoreContract::NAME, 'iLIKE', '%' . $request->search . '%')
                    ->orWhere(StoreContract::DESCRIPTION, 'iLIKE', '%' . $request->search . '%');
        })->paginate(20));
    }


    public function create(StoreRequest $request)
    {

    }


    public function store(StoreRequest $request)
    {
        $store = $this->storeRepository->create($request);

        return new StoreResource($store);
    }


    public function show(Store $store)
    {
        return new StoreResource($store);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Store  $store
     * @return \Illuminate\Http\Response
     */
    public function edit(Store $store)
    {

    }


    public function update(StoreRequest $request, Store $store)
    {
        return new StoreResource($this->storeRepository->update($request, $store));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Store  $store
     * @return \Illuminate\Http\Response
     */
    public function destroy(Store $store)
    {
        $this->storeRepository->delete($store);

        return response([
            'message' => 'Store successfully deleted'
        ]);
    }
}
