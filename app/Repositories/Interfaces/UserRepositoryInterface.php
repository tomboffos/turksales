<?php


namespace App\Repositories\Interfaces;


use App\Http\Requests\Auth\RegisterRequest;

interface UserRepositoryInterface
{
    public function create(RegisterRequest $request);
}
