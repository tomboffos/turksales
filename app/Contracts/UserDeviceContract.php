<?php


namespace App\Contracts;


interface UserDeviceContract
{
    const USER_ID = 'user_id';
    const DEVICE_TOKEN = 'device_token';

    const FILLABLE = [
        self::USER_ID,
        self::DEVICE_TOKEN
    ];
}
