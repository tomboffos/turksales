<?php

namespace App\Models;

use App\Contracts\ProductContract;
use App\Services\ProductService;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    protected $casts = [
        'images' => 'array'
    ];

    use HasFactory, SoftDeletes;

    protected $fillable = ProductContract::FILLABLE;

    public function setImagesAttribute($value)
    {
        $this->attributes['images'] = ProductService::saveImages($value);
    }

    public function store()
    {
        return $this->belongsTo(Store::class);
    }

}
