<?php


namespace App\Contracts;


interface CategoryContract
{
    const NAME = 'name';
    const IMAGE = 'image';

    const FILLABLE = [
        self::NAME,
        self::IMAGE
    ];
}
