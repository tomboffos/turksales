<?php

use App\Http\Controllers\Api\Auth\AuthController;
use App\Http\Controllers\Api\Store\CategoryController;
use App\Http\Controllers\Api\Store\ProductController;
use App\Http\Controllers\Api\Store\StoreController;
use App\Http\Controllers\Api\User\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return response(new \App\Http\Resources\UserResource($request->user()));
});

Route::prefix('/auth')->group(function () {
    Route::post('/login', [AuthController::class, 'login']);
    Route::post('/register', [AuthController::class, 'registerCustomer']);
    Route::post('/register/seller', [AuthController::class, 'registerSeller']);
});

Route::resources([
    'stores' => StoreController::class,
    'products' => ProductController::class,
    'categories' => CategoryController::class,
]);

Route::middleware('auth:sanctum')->group(function () {
    Route::prefix('/vendor')->group(function () {
        Route::resources([
            'stores' => StoreController::class,
            'products' => ProductController::class,
        ]);

        Route::get('/store', [StoreController::class, 'showOwnStore']);
        Route::get('/own-products',[ProductController::class,'showOwnProducts']);
    });
    Route::resources([
        'users' => UserController::class
    ]);
});
