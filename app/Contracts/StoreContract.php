<?php


namespace App\Contracts;


interface StoreContract
{
    const NAME        = 'name';
    const PHOTO       = 'photo';
    const DESCRIPTION = 'description';
    const USER_ID     = 'user_id';

    const FILLABLE = [
        self::NAME,
        self::PHOTO,
        self::DESCRIPTION,
        self::USER_ID
    ];
}
