<?php


namespace App\Repositories\Interfaces;


use App\Http\Requests\Product\CreateProductRequest;
use App\Http\Requests\Product\UpdateProductRequest;

interface ProductRepositoryInterface
{
    public function create(CreateProductRequest $request);

    public function update(UpdateProductRequest $request);
}
