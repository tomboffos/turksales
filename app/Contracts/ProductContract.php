<?php


namespace App\Contracts;


interface ProductContract
{
    const NAME = 'name';
    const IMAGES = 'images';
    const DESCRIPTION = 'description';
    const STORE_ID  = 'store_id';
    const CATEGORY_ID = 'category_id';
    const PRICE = 'price';

    const FILLABLE = [
        self::NAME,
        self::IMAGES,
        self::DESCRIPTION,
        self::STORE_ID,
        self::PRICE,
        self::CATEGORY_ID
    ];
}
