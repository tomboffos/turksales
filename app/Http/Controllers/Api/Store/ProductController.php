<?php

namespace App\Http\Controllers\Api\Store;

use App\Contracts\ProductContract;
use App\Http\Controllers\Controller;
use App\Http\Requests\Product\CreateProductRequest;
use App\Http\Resources\ProductResource;
use App\Models\Product;
use App\Repositories\ProductRepository;
use App\Repositories\StoreRepository;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    protected $productRepository;

    public function __construct()
    {
        $this->productRepository = new ProductRepository();
    }

    public function index(Request $request)
    {
        return ProductResource::collection(Product::where(function ($query) use ($request) {
            if ($request->has(ProductContract::STORE_ID))
                $query->where(ProductContract::STORE_ID, $request->store_id);
            if ($request->has(ProductContract::CATEGORY_ID))
                $query->where(ProductContract::CATEGORY_ID, $request->category_id);
            if ($request->has('search'))
                $query->where(ProductContract::NAME, 'iLIKE', '%' . $request->search . '%')
                    ->orWhere(ProductContract::DESCRIPTION, 'iLIKE', '%' . $request->search . '%');
        })->paginate(20));
    }

    public function show(Product $product)
    {
        return new ProductResource($product);
    }

    public function store(CreateProductRequest $request)
    {
        $product = $this->productRepository->create($request);

        return new ProductResource($product);
    }

    public function destroy(Product $product)
    {
        $product->delete();

        return response([],200);
    }

    public function showOwnProducts(Request $request)
    {
        return ProductResource::collection(Product::where('store_id',$request->user()->store->id)->orderBy('id','desc')->paginate(20));
    }
}
