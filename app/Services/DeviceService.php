<?php


namespace App\Services;


use App\Contracts\UserDeviceContract;
use App\Models\User;
use App\Models\UserDevice;

class DeviceService
{
    public function registerDevice($token, User $user)
    {
        if (!$user->checkIfTokenExists($token, $user))
            UserDevice::updateOrCreate([
                UserDeviceContract::USER_ID => $user->id,
            ], [
                UserDeviceContract::DEVICE_TOKEN => $token
            ]);

    }
}
