<?php

namespace App\Models;

use App\Contracts\StoreContract;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Store extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = StoreContract::FILLABLE;

    public function product()
    {
        return $this->hasMany(Product::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function setPhotoAttribute($value)
    {
        $this->attributes[StoreContract::PHOTO] = $value != null ?  str_replace('public', '', asset('storage' . $value->store('public/messages/medias'))) : $this->attributes[StoreContract::PHOTO];
    }
}
