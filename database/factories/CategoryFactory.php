<?php

namespace Database\Factories;

use App\Contracts\CategoryContract;
use App\Models\Category;
use Illuminate\Database\Eloquent\Factories\Factory;

class CategoryFactory extends Factory
{
    protected $model = Category::class;
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
            CategoryContract::NAME => $this->faker->name,
            CategoryContract::IMAGE => $this->faker->imageUrl,
        ];
    }
}
