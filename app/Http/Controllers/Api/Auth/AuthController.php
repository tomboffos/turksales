<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\LoginRequest;
use App\Http\Requests\Auth\RegisterRequest;
use App\Http\Requests\Auth\SellerRegisterRequest;
use App\Http\Resources\UserResource;
use App\Models\User;
use App\Repositories\UserRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class AuthController extends Controller
{
    protected $userRepository;

    public function __construct()
    {
        $this->userRepository = new UserRepository();
    }

    public function registerCustomer(RegisterRequest $request)
    {
        $user = $this->userRepository->create($request);

        return response([
            'user' => new UserResource($user),
            'token' => $user->createToken(Str::random(10))->plainTextToken
        ], 201);
    }


    public function registerSeller(SellerRegisterRequest $request)
    {
        $user = User::create(array_merge($request->all(), ['role_id' => 2]));

        return response([
            'user' => new UserResource($user),
            'token' => $user->createToken(Str::random(10))->plainTextToken
        ], 201);
    }


    public function login(LoginRequest $request)
    {
        $user = User::where('email', $request->email)->first();
        if (Auth::attempt($request->validated())) {
            return response([
                'user' => new UserResource($user),
                'token' => $user->createToken(Str::random(10))->plainTextToken
            ], 200);
        } else {
            return response([
                'message' => 'Incorrect email or password'
            ], 400);
        }
    }
}
