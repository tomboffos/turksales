<?php


namespace App\Repositories;


use App\Contracts\ProductContract;
use App\Http\Requests\Product\CreateProductRequest;
use App\Http\Requests\Product\UpdateProductRequest;
use App\Models\Product;
use App\Repositories\Interfaces\ProductRepositoryInterface;

class ProductRepository implements ProductRepositoryInterface
{
    public function create(CreateProductRequest $request)
    {
        return Product::create(array_merge($request->validated(),[
            ProductContract::STORE_ID => $request->user()->store->id
        ]));
    }

    public function update(UpdateProductRequest $request)
    {
        // TODO: Implement update() method.
    }
}
