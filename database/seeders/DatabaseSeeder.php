<?php

namespace Database\Seeders;

use App\Contracts\CategoryContract;
use App\Models\Category;
use App\Models\Product;
use App\Models\Store;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         \App\Models\User::factory(500)->create();
        Store::factory(500)->create();
        Category::factory(50)->create();
        Product::factory(10000)->create();
    }
}
