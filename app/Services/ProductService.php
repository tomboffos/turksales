<?php


namespace App\Services;


class ProductService
{
    public static function saveImages($values)
    {
        $imagesMain = [];

        foreach ($values as $image)
            $imagesMain[] = is_string($image) ? $image : str_replace('public', '', asset('storage' . $image->store('public/messages/medias')));

        return json_encode($imagesMain);
    }

}
