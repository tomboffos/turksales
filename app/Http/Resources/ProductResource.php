<?php

namespace App\Http\Resources;

use App\Contracts\ProductContract;
use Illuminate\Http\Resources\Json\JsonResource;

class ProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            ProductContract::NAME => $this->name,
            ProductContract::IMAGES => $this->images,
            ProductContract::DESCRIPTION => $this->description,
            ProductContract::PRICE => $this->price,
            'user' => $this->store->user,
            'id' => $this->id
        ];
    }
}
