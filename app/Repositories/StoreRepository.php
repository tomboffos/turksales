<?php


namespace App\Repositories;


use App\Contracts\StoreContract;
use App\Http\Requests\Store\StoreRequest;
use App\Models\Store;
use App\Repositories\Interfaces\StoreRepositoryInterface;

class StoreRepository implements StoreRepositoryInterface
{
    public function create(StoreRequest $request)
    {
        return Store::create(array_merge($request->validated(), [
            StoreContract::USER_ID => $request->user()->id
        ]));
    }

    public function update(StoreRequest $request, Store $store)
    {
        $store->update($request->validated());
        return $store;
    }

    public function delete(Store $store)
    {
        return $store->delete();
    }
}
