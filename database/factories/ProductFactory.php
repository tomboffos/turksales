<?php

namespace Database\Factories;

use App\Contracts\ProductContract;
use App\Models\Category;
use App\Models\Product;
use App\Models\Store;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProductFactory extends Factory
{
    protected $model = Product::class;
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
            ProductContract::NAME => $this->faker->name,
            ProductContract::DESCRIPTION => $this->faker->text,
            ProductContract::PRICE => mt_rand(1000,100000),
            ProductContract::STORE_ID => Store::inRandomOrder()->first()->id,
            ProductContract::IMAGES => [
                $this->faker->imageUrl,
                $this->faker->imageUrl
            ],
            ProductContract::CATEGORY_ID => Category::inRandomOrder()->first()->id
        ];
    }
}
